'use strict';

/**
 * @ngdoc function
 * @name initApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the initApp
 */
angular.module('tasksApp')
.controller('SignupCtrl', ['$scope', function($scope) {
    $scope.submitForm = function(data) {
        console.log('I\'m trying to submit...');
        var dataConstruct = ('name=' + data.name + '&email=' + data.email + '&password=' + data.password);
            console.log(dataConstruct);
            /*$http({method: 'POST',
                   url: apiUrl,
                   data: dataConstruct,
                   headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                  }).
            success(function(data, status) {
                // TODO: Error handling is still not great
                console.log('Successful!');
                $scope.formSubmit.greatSuccess = "Tasks successfully submitted";
                $scope.formSubmit.inputs = [];
            }).
            error(function(data, status) {
                console.log('Not so successful!');
                $scope.formSubmit.failed = "Unable to submit taks at this time!";
            });*/
    };
}])


.directive('match', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        scope: {
            match: '='
        },
        link: function(scope, elem, attrs, ctrl) {
            scope.$watch(function() {
                var modelValue = ctrl.$modelValue || ctrl.$$invalidModelValue;
                return (ctrl.$pristine && angular.isUndefined(modelValue)) || scope.match === modelValue;
            }, function(currentValue) {
                ctrl.$setValidity('match', currentValue);
            });
        }
    };
});
