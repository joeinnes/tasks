'use strict';

/**
 * @ngdoc function
 * @name initApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the initApp
 */
angular.module('tasksApp')
.controller('ViewTasksCtrl', function ($scope, $http, user) {
    user.getCurrent().then(function(currentUser) {
        $http({
            url: apiUrl + "creator/" + currentUser.first_name + " " + currentUser.last_name + "/",
            method: "GET",
            params: {
                'by': 'timestamp',
                'order': 'desc',
                'limit': '50',
            }
        }).
        success(function(data, status, headers, config) {
            $scope.tasks = data;
        }).
        error(function(data, status, headers, config) {
            // TODO: Error log function
        });
    });
});
