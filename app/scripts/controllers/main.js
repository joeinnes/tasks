'use strict';

/**
 * @ngdoc function
 * @name initApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the initApp
 */
angular.module('tasksApp')
.controller('MainCtrl', function ($scope, $http, user) {
    user.getCurrent().then(function(currentUser) {

        console.log('Logged in user is:' + currentUser.email); // TODO: Use this to search API for user's tasks
        // TODO: Set up API to allow searching in this way

        $scope.formSubmit = {
            inputs: [],
            input_new: { creator: currentUser.first_name + ' ' + currentUser.last_name, timestamp: Math.round(new Date().getTime() / 1000), task: '' }
        };

        $scope.formClear = function() {
            $scope.formSubmit.inputs = [];
            $scope.formSubmit.input_new = "";
            console.log('Form cleared');
        }

        $scope.add = function() {
            // add the new option to the model FIXME: Only if not empty
            if ($scope.formSubmit.input_new.task) {
                $scope.formSubmit.inputs.push($scope.formSubmit.input_new);
            }
            else {
                $scope.formSubmit.failed = "No tasks to submit";
            }
            // clear the option.
            $scope.formSubmit.input_new = { creator: currentUser.first_name + currentUser.last_name, timestamp: Math.round(new Date().getTime() / 1000), task: '' };
        };

        $scope.remove = function(index){
            $scope.formSubmit.inputs.splice(index, 1);
        };

        $scope.submitForm = function(data) {
            $scope.add();
            // use a variable dataConstruct to construct the payload
            angular.forEach(data, function(value, key) {
                var dataConstruct = ('creator=' + value.creator + '&timestamp=' + value.timestamp + '&task=' + value.task);
                console.log(dataConstruct);
                $http({method: 'POST',
                       url: apiUrl,
                       data: dataConstruct,
                       headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                      }).
                success(function(data, status) {
                    // TODO: Error handling is still not great
                    console.log('Successful!');
                    $scope.formSubmit.greatSuccess = "Tasks successfully submitted";
                    $scope.formClear();
                }).
                error(function(data, status) {
                    console.log('Not so successful!');
                    $scope.formSubmit.failed = "Unable to submit tasks at this time!";
                });
            });
        };

        $('#my-alert').on('closed.bs.alert', function () {
            $scope.formSubmit.greatSuccess = "";
            $scope.formSubmit.failed = "";
        })
    });
})
