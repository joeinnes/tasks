'use strict';

/**
 * @ngdoc overview
 * @name tasksApp
 * @description
 * # tasksApp
 *
 * Main module of the application.
 */
angular
.module('tasksApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'UserApp'
])
.config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        public: true
    })
    .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        public: true
    })
    .when('/view-tasks', {
        templateUrl: 'views/view-tasks.html',
        controller: 'ViewTasksCtrl'
    })
    .when('/login', {
        templateUrl: 'views/login.html',
        login: true
    })
    .when('/signup', {
        templateUrl: 'views/signup.html',
        controller: 'SignupCtrl',
        public: true
    })
    .otherwise({
        redirectTo: '/'
    });
})

.run(function(user) {
    user.init({
        appId: '5446ad0311e7a',
        heartbeatInterval: 0
    });
})

.controller('HeaderCtrl', function ($scope, $location) {
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
})

var apiUrl = "http://192.168.55.55/api/tasks/";
// var userName = "Joe Innes" // TODO: Username management via cookies/login
