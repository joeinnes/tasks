-- phpMyAdmin SQL Dump
-- version 4.2.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 16, 2014 at 10:03 AM
-- Server version: 5.5.40-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tasks`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `creator` text NOT NULL,
  `task` text NOT NULL,
  `timestamp` int(11) NOT NULL,
   PRIMARY KEY ( id )
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`creator`, `task`, `timestamp`) VALUES
('Joe Innes', 'Built the first version of Tasks and uploaded it', 1413834440),
('Joe Innes', 'Created Verizon info batch file', 1413835489),
('Joe Innes', 'Nav pills showing active class correctly', 1413917312),
('Joe Innes', 'Updated favicon, touch icons, and lead icon', 1413917323),
('Joe Innes <joeinnes@gmail.com>', 'Testing API filter', 1413922946);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
