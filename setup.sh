#!/bin/bash
sudo apt-get -y update
sudo apt-get -y install apache2 php5 libapache2-mod-php5
sudo service apache2 restart
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password rootpassword'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password rootpassword'
sudo apt-get -y install mysql-server
